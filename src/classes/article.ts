export class Article {
    id: number;
    name: string;
    description: string;
    rating: number;
    category: number;
    pic: string;
}


import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Article } from '../../classes/article';
import { Category } from '../../classes/category';
import { ArticlesService } from  '../../providers/articles';
import { CategoriesService } from  '../../providers/categories';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    articles: Article[];
    categorizedarticles: Observable<Article[]>;
    categories: Category[];
    currentcategory: number = -1;
    icons = ['','sunny','moon','people'];

    constructor(
        public navCtrl: NavController,
        private articlesProvider: ArticlesService,
        private categoriesProvider: CategoriesService) {

    }

    /**
     * Gets all articles from database
     */
    getArticles(): void {
        this.articlesProvider.getArticles().then(articles => {
            this.articles = articles;
        });
    }

    /**
     * Gets articles given a category
     */
    getArticlesByCategory(): void {
        this.categorizedarticles = this.articlesProvider
                .getArticlesByCategory(this.currentcategory);
    }

    /**
     * Gets categories
     */
    getCategories(): void {
        this.categoriesProvider.getCategories().then(categories => {
            this.categories = categories;
        });
    }

    /**
     * Helper function to create a rating array, used to print start
     *
     * @param number
     * @returns {number[]}
     */
    createRating(number) {
        let items: number[] = [];
        for(let i = 1; i <= number; i++){
            items.push(i);
        }
        return items;
    }

    /**
     * Sets current category
     *
     * @param categoryid
     */
    setCurrentCategory(categoryid) {
        this.currentcategory = categoryid;
        this.getArticlesByCategory();
    }

    /**
     * Opens an article
     *
     * @param articleid
     */
    openDetails(articleid) {
        this.navCtrl.setRoot('Details', {
            'id': articleid
        });
    }

    ionViewWillEnter() {
        this.getArticlesByCategory();
        this.getCategories();
    }

}

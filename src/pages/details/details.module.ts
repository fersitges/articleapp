import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Details } from './details';
import { ArticlesService } from "../../providers/articles";
import { CategoriesService } from  '../../providers/categories';

@NgModule({
    declarations: [
        Details
    ],
    imports: [
        IonicPageModule.forChild(Details)
    ],
    exports: [
        Details
    ],
    providers: [
        ArticlesService,
        CategoriesService
    ]
})
export class DetailsModule {
}

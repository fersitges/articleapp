import { Component } from '@angular/core';
import { NavParams } from 'ionic-angular';
import { ArticlesService } from  '../../providers/articles';
import { Http } from '@angular/http';
import { LoadingController } from 'ionic-angular';
import { ViewController } from 'ionic-angular';

@Component({
    selector: 'rate-modal',
    templateUrl: 'ratemodal.html',
    providers: [ ArticlesService ]
})
export class RateModal {
    articleid: number;
    currentrating: number = 1;

    constructor (private loader: LoadingController,
                 public http: Http,
                 private articlesProvider: ArticlesService,
                 public viewCtrl: ViewController,
                 public params: NavParams) { }

    rateArticle() {
        let loading = this.loader.create({
            content: 'Please wait...'
        });

        loading.present();
        this.articlesProvider
            .rateArticle(this.articleid, this.currentrating)
            .subscribe(ratingresponse => {
                console.log(ratingresponse);
                loading.dismiss();
                this.viewCtrl.dismiss();
            });
    }

    ionViewDidLoad() {
        this.articleid = this.params.get('articleid');
    }
}
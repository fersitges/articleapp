import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Article } from '../../classes/article';
import { ArticlesService } from  '../../providers/articles';
import { CategoriesService } from  '../../providers/categories';
import { Http } from '@angular/http';
import { Category } from '../../classes/category';
import { Rating } from '../../classes/rating';
import { ModalController } from 'ionic-angular';
import { RateModal } from './ratemodal';

@IonicPage({
    segment: 'details/:id'
})
@Component({
    selector: 'page-details',
    templateUrl: 'details.html',
    providers: [ ArticlesService, CategoriesService ]
})
export class Details {
    article: Article;
    category: Category;
    currentid: number;
    articleratings: Rating[];
    ratingscount: number;
    ratingsaverage: number;

    constructor(public navCtrl: NavController,
                public navParams: NavParams,
                public http: Http,
                private articlesProvider: ArticlesService,
                private categoriesProvider: CategoriesService,
                public modalCtrl: ModalController) {
        this.currentid = navParams.get('id');
    }

    /**
     * Gets an item from database
     */
    getArticle() {
        this.articlesProvider
            .getSingleArticle(this.currentid)
            .subscribe(article => {
                this.article = article[0];
                this.getCategory(this.article.category);
                this.getArticleRating();
            });
    }

    /**
     * Gets article rating based on average
     */
    getArticleRating() {
        this.articlesProvider
            .getArticleRating(this.article.id)
            .subscribe(ratings => {
                this.articleratings = ratings;
                this.ratingscount = ratings.length;
                if (this.ratingscount) {
                    let sum = 0;
                    ratings.forEach((rating) => {
                        sum += rating.rating;
                    });
                    this.ratingsaverage = Math.round(sum / this.ratingscount);
                    console.log(this.ratingscount, this.ratingsaverage);
                } else this.ratingsaverage = 0;
            });
    }

    /**
     * Opens rating modal, then gets the article rating
     */
    rateArticle() {
        let ratingModal = this.modalCtrl.create(RateModal, {
            articleid: this.article.id
        });

        // Preparar callback cuando la modal sea cerrada
        ratingModal.onDidDismiss(() => {
            this.getArticleRating();
        });

        ratingModal.present();
    }

    /**
     * Helper function to create a rating array, used to print start
     *
     * @param number
     * @returns {number[]}
     */
    createRating(number) {
        let items: number[] = [];
        for(let i = 1; i <= number; i++){
            items.push(i);
        }
        return items;
    }

    /**
     * Gets category
     * @param categoryid
     */
    getCategory(categoryid) {
        this.categoriesProvider
            .getCategory(categoryid)
            .subscribe(category => this.category = category[0]);
    }

    ionViewDidLoad() {
        this.getArticle();
    }

}



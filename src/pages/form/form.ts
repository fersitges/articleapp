import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Category } from '../../classes/category';
import { CategoriesService } from  '../../providers/categories';
import { Article } from '../../classes/article';
import { ArticlesService } from "../../providers/articles";

@Component({
    selector: 'page-form',
    templateUrl: 'form.html'
})
export class FormPage {
    categories: Category[];
    character: Article = {
        id: 0,
        name: '',
        description: '',
        rating: 1,
        category: 0,
        pic: ''
    };

    constructor(public navCtrl: NavController,
                private categoriesProvider: CategoriesService,
                private articlesProvider: ArticlesService) {

    }

    /**
     * Gets categories
     */
    getCategories(): void {
        this.categoriesProvider.getCategories().then(categories => {
            this.categories = categories;
        });
    }

    /**
     * Adds a new article
     */
    addArticle(): void {
        let name = this.character.name.trim();
        if (!name) { return; }
        this.articlesProvider.createArticle(this.character)
            .then(article => {
                console.log(article);
                this.navCtrl.setRoot('Details', {
                    'id': article.id
                });
        });
    }

    ionViewWillEnter() {
        this.getCategories();
    }
}


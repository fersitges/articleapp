import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { Article } from '../../classes/article';
import { ArticleSearchService } from  '../../providers/articlessearch';
import { Subject } from 'rxjs/Subject';
import { Observable } from "rxjs/Observable";

// Observable class extensions
import 'rxjs/add/observable/of';
// Observable operators
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/debounceTime';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/switchMap';

@Component({
    selector: 'page-list',
    templateUrl: 'list.html'
})
export class ListPage {
    filteredArticles: Observable<Article[]>;
    private searchTerms = new Subject<string>();

    constructor(public navCtrl: NavController,
                private articleSearchService: ArticleSearchService) { }

    /**
     * Push search term into observable stream
     *
     * @param {string} term
     */
    search(term: string): void {
        this.searchTerms.next(term);
    }

    /**
     * Opens details view from results
     *
     * @param articleid
     */
    openDetails(articleid) {
        this.navCtrl.setRoot('Details', {
            'id': articleid
        });
    }
    
    ionViewWillEnter() {
        this.filteredArticles = this.searchTerms
            .debounceTime(300)
            .distinctUntilChanged()  // ignore if next search term is same as previous
            .switchMap(term => term  // switch to new observable each time the term changes
                // return the http observable
                ? this.articleSearchService.search(term)
                // or the observable of empty articles if there was no search term
                : Observable.of<Article[]>([]) )
            .catch(error => {
                // TODO: add real error handling
                console.log(error);
                return Observable.of<Article[]>([]);
            });
    }

}

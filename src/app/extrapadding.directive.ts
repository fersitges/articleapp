import { Directive, ElementRef } from '@angular/core';

@Directive({ selector: '[extrapadding]' })
export class ExtrapaddingDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.paddingTop = '10px';
    console.log(
      `* AppRoot highlight called for ${el.nativeElement.tagName}`);
  }
}

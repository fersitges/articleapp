import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { ListPage } from '../pages/list/list';
import { FormPage } from '../pages/form/form';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { InMemoryWebApiModule } from 'angular-in-memory-web-api';
import { InMemoryDataService } from '../providers/in-memory-data.service';
import { ArticlesService } from '../providers/articles';
import { ArticleSearchService } from '../providers/articlessearch';
import { HttpModule } from "@angular/http";
import { CategoriesService } from "../providers/categories";
import { RateModal } from '../pages/details/ratemodal';
import { ExtrapaddingDirective } from './extrapadding.directive';

@NgModule({
    declarations: [
        MyApp,
        HomePage,
        ListPage,
        FormPage,
        RateModal,
        ExtrapaddingDirective
    ],
    imports: [
        BrowserModule,
        IonicModule.forRoot(MyApp),
        HttpModule,
        InMemoryWebApiModule.forRoot(InMemoryDataService),
    ],
    bootstrap: [IonicApp],
    entryComponents: [
        MyApp,
        HomePage,
        ListPage,
        RateModal,
        FormPage
    ],
    providers: [
        StatusBar,
        SplashScreen,
        ArticlesService,
        CategoriesService,
        ArticleSearchService,
        {provide: ErrorHandler, useClass: IonicErrorHandler}
    ]
})
export class AppModule {
}

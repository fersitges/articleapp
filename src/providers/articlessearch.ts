import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Article } from '../classes/article';
import 'rxjs/add/operator/map';

@Injectable()
export class ArticleSearchService {
    private articlesUrl = 'api/articles';

    constructor(private http: Http) {}

    search(term: string): Observable<Article[]> {
        return this.http
            .get(this.articlesUrl + `?name=${term}`)
            .map(response => response.json().data as Article[]);
    }

}
import {Injectable} from '@angular/core';
import {Http, Response} from '@angular/http';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import { Category } from '../classes/category';
import {Observable} from "rxjs/Observable";

@Injectable()
export class CategoriesService {

    private categoriesUrl = 'api/categories';

    constructor(public http: Http) {
    }

    getCategories(): Promise<Category[]> {
        return this.http.get(this.categoriesUrl)
            .toPromise()
            .then(response => response.json().data as Category[])
            .catch(this.handleError);
    }

    getCategory(categoryid): Observable<Category> {
        let urlparams = `?categoryid=${categoryid}`;

        return this.http
            .get(this.categoriesUrl + urlparams)
            .map(response => response.json().data as Category)
            .catch(this.handleError);
    }

    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Promise.reject(errMsg);
    }

}
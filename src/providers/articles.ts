import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';
import 'rxjs/add/operator/catch';

import { Article } from '../classes/article';
import {Rating} from "../classes/rating";

@Injectable()
export class ArticlesService {
    private articlesUrl = 'api/articles';
    private ratingsUrl = 'api/ratings';

    constructor(public http: Http) {    }

    /**
     * Gets articles from database. Promise.
     *
     * @returns {Promise<ErrorObservable<any>|TResult2|Article[]>}
     */
    getArticles(): Promise<Article[]> {
        return this.http.get(this.articlesUrl)
            .toPromise()
            .then(response => response.json().data as Article[])
            .catch(this.handleError);
    }

    /**
     * Gets an article observable
     *
     * @param articleid
     * @returns {Observable<R>}
     */
    getSingleArticle(articleid): Observable<Article> {
        let urlparams = `?id=${articleid}`;

        return this.http
            .get(this.articlesUrl + urlparams)
            .map(response => response.json().data as Article)
            .catch(this.handleError);
    }

    /**
     * Gets articles by category, observable
     *
     * @param {number} categoryid
     * @returns {Observable<R>}
     */
    getArticlesByCategory(categoryid: number): Observable<Article[]> {
        let urlparams = '';
        if (categoryid > -1) {
            urlparams = `?category=${categoryid}`;
        }
        return this.http
            .get(this.articlesUrl + urlparams)
            .map(response => response.json().data as Article[])
            .catch(this.handleError);
    }

    /**
     * Saves user rating in database
     *
     * @param {number} articleid
     * @param {number} rating
     * @returns {Observable<R>}
     */
    rateArticle(articleid: number, rating: number): Observable<Article> {
        let thisRating: Rating = { articleid: articleid, rating: rating };
        let headers = new Headers({ 'Content-Type': 'application/json' });
        let options = new RequestOptions({ headers: headers });

        return this.http
            .post(this.ratingsUrl, thisRating, options)
            .map(this.extractData)
            .catch(this.handleError);
    }

    /**
     * Gets an article rating
     *
     * @param {number} articleid
     * @returns {Observable<R>}
     */
    getArticleRating(articleid: number): Observable<Rating[]> {
        let urlparams = `?articleid=${articleid}`;
        return this.http
            .get(this.ratingsUrl + urlparams)
            .map(response => response.json().data as Rating[])
            .catch(this.handleError);
    }

    createArticle(newArticle): Promise<Article> {
        let headers = new Headers({ 'Content-Type': 'application/json' });

        return this.http
            .post(this.articlesUrl, JSON.stringify(newArticle), {headers: headers})
            .toPromise()
            .then(res => res.json().data as Article)
            .catch(this.handleError);
    }

    /**
     * Helper funcion to extract data from json result
     *
     * @param {Response} res
     * @returns {{}}
     */
    private extractData(res: Response) {
        let body = res.json();
        console.log(body);
        return body.data || { };
    }

    /**
     * Helper function to handle exceptions
     *
     * @param {Response|any} error
     * @returns {ErrorObservable<T>}
     */
    private handleError(error: Response | any) {
        // In a real world app, we might use a remote logging infrastructure
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
//        return Promise.reject(errMsg);
        return Observable.throw(errMsg);
    }

}
